# -*- coding: utf-8 -*-
"""
Created on Sat Nov 03 15:46:18 2012

@author: Jim

Functions:

simGBM - simulate GBM
simGBMvec - simulate a vector version of GBM
simVAR - simulate vector autoregressive model
simBrownian - simulate a Brownian motion
simPoisson - simulate a Poisson process
simOU - simulate an OU process
simCIR - simulate a CIR process


"""


import numpy as np
import pandas as pd




#%%
def simPoisson(lam,T,dt,x0,numsims=1):
    """
    simPoisson(lam,T,dt,x0,numsims = 1)
    
    simulate a Poisson Process with intensity lam
    """
    N = int(T/dt)
    timepts = dt*np.mat(np.arange(N+1)).T
    p = lam*dt  # probability of a jump
    dy = np.random.binomial(1,p,(len(timepts),1))
    y = x0 + np.cumsum(dy,0)
    
    return y, timepts


#%%
def simBrownian(mu,sig,T,dt,x0,numsims=1):
    """
    simBrownian(mu,sig,T,dt,x0,numsims = 1)
    
    simulate a Brownian motion with drift mu and 
    volatility sig.
    """
    N = int(T/dt)
    timepts = dt*np.mat(np.arange(N+1)).T
    mudt = mu*timepts
    sigdZ = sig*np.cumsum(np.r_[np.zeros((1,numsims)),np.random.randn(N,numsims)*np.sqrt(dt)],0)
    y = x0 + np.kron(np.ones((1,numsims)),mudt) + sigdZ
    return y, timepts



#%%
def simOU(kappa,theta,sig,T,N,x0,numsims=1,df_out = True):
    """
    simOU(kappa,theta,sig,T,N,x0,numsims = 1)
    
    simulate an OU process 
    
    N is the number of time steps
    
    dx = kappa*(theta - x)dt + sig*dz
    
    """
    #N = int(T/dt)
    dt = float(T)/float(N)
    timepts = dt*np.mat(np.arange(N+1)).T
    y = np.ones((N+1,numsims))

    dz = np.sqrt(dt)*np.random.randn(N,numsims)

    for j in range(numsims):    
        y[0,j] = x0
        for i in range(N):
            y[i+1,j] = y[i,j] + kappa*(theta - y[i,j])*dt + sig*dz[i,j]
            
    if df_out == True:
        y = pd.DataFrame(y)
    
    return y, timepts



#%%
def simCIR(kappa,theta,sig,T,dt,x0,numsims=1):
    """
    simOU(kappa,theta,sig,T,dt,x0,numsims = 1)
    
    simulate an OU process 
    
    dx = kappa*(theta - x)dt + sig*dz
    
    """
    N = int(T/dt)
    timepts = dt*np.mat(np.arange(N+1)).T
    y = np.ones((N+1,1))

    dz = np.sqrt(dt)*np.random.randn(N,1)

    y[0] = x0
    for i in range(N):
        y[i+1,0] = y[i,0] + kappa*(theta - y[i,0])*dt + sig*np.sqrt(y[i,0])*dz[i,0]
        

    return y, timepts




#%%
def simGBM(mu,sig,T,N,x0,numsims=1):
    """
    simulate a geometric Brownian motion with drift mu and 
    volatility sig.
    
    inputs
    mu - drift
    sig - volatility
    T - final time in years
    N - number of time steps
    x0 - initial condition
    numsims - number of simulations
    
    OUTPUTs
    yy - paths
    timepts - time points
    
    J. Primbs
    6/1/2017
    """
    #N = int(T/dt)
    dt = T/N
    timepts = dt*np.array(np.arange(N+1))
    nu = mu-(0.5)*sig**2
    nudt = nu*timepts
    sigdZ = sig*np.cumsum(np.vstack([np.zeros((1,numsims)),np.random.randn(N,numsims)*np.sqrt(dt)]),0)
    yy = np.kron(np.ones((numsims,1)),nudt).T + sigdZ
    y = x0*np.exp(yy)
    return y, timepts
    



#%%
def simGBMvec(mu,Cov,T,dt,x0,numsims):
    """
    simulate a vector GBM with covariance matrix Cov
     
     Inputs
    
    
     Outputs
    
    
     J. Primbs
     11/5/2012
    """
    m = np.size(Cov,0) # number of states
    N = int(T/dt)
    timepts = dt*np.mat(np.arange(0,N+1)).T
    nu = mu-0.5*np.mat(np.diag(Cov)).T
    nudt = np.kron(nu.T,timepts)
    L = np.linalg.cholesky(Cov)  # L*L.T
    # create a vector brownian motion
    dZ = np.r_[np.zeros((1,m)),np.random.randn(N,m)*np.sqrt(dt)]
    LdZ = dZ.dot(L.T)
    Z = np.cumsum(LdZ,0)
    y = np.multiply(np.kron(x0.T,np.ones((N+1,1))),np.exp(nudt+Z)) 
    return y, timepts
    
    
    
    
#%%
def simVARold(alpha0, alpha1, C, x0, numtimesteps, numsims, Cov = None):
    """
    simulate the vector autoregressive model (VAR) given by
    
    x(k+1) = alpha0 + alpha1*x(k) + C*z(k)
    
    where z(k) is distributed N(0,Cov)
    
    J. Primbs
    10/1/2015
    """
    
    alpha0 = np.mat(alpha0)
    alpha1 = np.mat(alpha1)    
    C = np.mat(C)    
    
    n = np.size(alpha1,0)  # size of the state
    p = np.size(C,1)   # number of random inputs z
    
    if not Cov == None:
        L = np.cholesky(np.mat(Cov))
        C = C*L
    
    #k = np.arange(numtimesteps)
    x = np.mat(np.zeros((n,numtimesteps+1))) # initialize x
        
    x[:,0] = np.mat(x0)
    
    z = np.mat(np.random.standard_normal((p*numsims,numtimesteps)))
    
    for j in range(numsims):
        for i in range(numtimesteps):
            #z = np.mat(np.random.randn(p,1))
            x[j*n:(j+1)*n,i+1] = alpha0 + alpha1*x[j*n:(j+1)*n,i] + C*z[j*p:(j+1)*p,i]
        
    return x
    
    
#%%
def simVAR(alpha0, alpha1, C, x0, numtimesteps, numsims, Cov = None):
    """
    simulate the vector autoregressive model (VAR) given by
    
    x(k+1) = alpha0 + alpha1*x(k) + C*z(k)
    
    where z(k) is distributed N(0,Cov)
    
    J. Primbs
    10/1/2015
    """
    
    alpha0 = np.mat(alpha0)
    alpha1 = np.mat(alpha1)    
    C = np.mat(C)    
    
    n = np.size(alpha1,0)  # size of the state
    p = np.size(C,1)   # number of random inputs z
    
    if not Cov == None:
        L = np.cholesky(np.mat(Cov))
        C = C*L
    
    # initialize the x variable
    x = np.zeros((n,numtimesteps+1,numsims)) # initialize x
        
    # create N(0,1) random variables
    z = np.random.standard_normal((p,numtimesteps,numsims))
    
    # loop and create the simulation paths
    for j in range(numsims):
        x[:,0:1,j] = np.mat(x0)    
        for i in range(numtimesteps):
            #z = np.mat(np.random.randn(p,1))
            x[:,i+1:i+2,j] = alpha0 + np.dot(alpha1,x[:,i:i+1,j]) + np.dot(C,z[:,i:i+1,j])
        
    return x
        



#%%
def simVAR2(alpha0, alpha1, beta, x0, numtimesteps, numsims, Cov = None, panel_out = False):
    """
    simulate the vector autoregressive model (VAR) given by
    
    x(k+1) = alpha0 + alpha1*x(k) + beta*z(k)
    
    where z(k) is distributed N(0,Cov)
    
    if panel_out = True, then the output is in the form of a pandas Panel data object
    
    the default is panel_out = False.  

    The output is a 3-d array:
    
    x[numsims,time_axis,state_axis]    
    
    J. Primbs
    10/1/2015
    """
    
    alpha0 = np.mat(alpha0)
    alpha1 = np.mat(alpha1)    
    beta = np.mat(beta)    
    
    n = np.size(alpha1,0)  # size of the state
    p = np.size(beta,1)   # number of random inputs z
    
    if not Cov == None:
        L = np.cholesky(np.mat(Cov))
        beta = beta*L
    
    # initialize the x variable
    x = np.zeros((numsims,numtimesteps+1,n)) # initialize x
        
    # create N(0,1) random variables
    z = np.random.standard_normal((numsims,numtimesteps,p))
    
    # loop and create the simulation paths
    for j in range(numsims):
        x[j,0:1,:] = np.mat(x0).T    
        for i in range(numtimesteps):
            #z = np.mat(np.random.randn(p,1))
            x[j,i+1:i+2,:] = (alpha0 + np.dot(alpha1,x[j,i:i+1,:].T) + np.dot(beta,z[j,i:i+1,:].T)).T
        
        
    if panel_out == True:
        x = pd.Panel(x)
        
        
    return x
    

    
    


    
    
    