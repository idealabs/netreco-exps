# -*- coding: utf-8 -*-
"""
Created on Thu Jun 29 10:54:21 2017

@author: japri
"""

#%%
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import SimMod as sim

#%% run a simulation

dt = 1./250.  # daily time step
numsims = 1
numtimesteps = 1000
n = 10  # number of stocks

#x0 = np.array([[0., 0., 0.]]).T



#%% Cointegration parameters
kappa = 2.  # mean reversion rate
theta = 3 # mean reversion level
sig = 1  # volatility of cointegration


#%% Simulate the spread
T = dt*numtimesteps
S0 = 0
S,timepts = sim.simOU(kappa,theta,sig,T,numtimesteps,S0,numsims,df_out = False)

#S2,timepts2 = sim.simOU(kappa,theta,sig,T,numtimesteps,S0,numsims,df_out = False)


#%% Simulate a GBM
mu_gbm = 0.3
sig_gbm = 0.4
x0 = 10.  # initial condition of the stock
X,timepts_gbm = sim.simGBM(mu_gbm,sig_gbm,T,numtimesteps,x0,numsims)

#%% Simulate a vector GBM
mu_vec = 0.1*np.ones((2,1))
corrmat = np.array([[1,0.9],[0.9,1]])
sigvec = np.array([0.4,0.4])
Cov = (sigvec.T@sigvec)*corrmat
x0vec = np.array([[10.,13.]]).T

Z, timepts_vec = sim.simGBMvec(mu_vec,Cov,T,dt,x0vec,numsims)

#%% Create the stock prices
dS = pd.DataFrame(S)

df = pd.DataFrame(Z)
#df[2] = X
df[2] = df[1]+dS[0]

#%% Save the results
df.to_csv('C:/Users/Jim/OneDrive/Python/MyCode/BYU Project/Network Reconstruction/SimStockCoint3.csv')

