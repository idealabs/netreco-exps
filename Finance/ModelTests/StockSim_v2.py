# -*- coding: utf-8 -*-
"""
Created on Thu Jun 29 10:54:21 2017

@author: japri
"""

#%%
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import SimMod as sim

#%% run a simulation

dt = 1./252.  # daily time step
numsims = 1
numtimesteps = 1000
n = 10  # number of stocks

#x0 = np.array([[0., 0., 0.]]).T

x0 = np.zeros((n,1))

muvec = (np.random.rand(n,1) - 0.5*np.ones((n,1)))/2.
sigvec = np.random.rand(n,1)/2.

Ctemp = np.random.rand(n,n)
C = Ctemp.T@Ctemp
Cnew = np.zeros((n,n))


for i in range(n):
    for j in range(n):
        Cnew[i,j] = C[i,j]/np.sqrt(C[i,i]*C[j,j])
        
corrmat = Cnew
#corrmat = np.random.rand(10,10)/1.1
#corrmat = corrmat.T@corrmat
#for i in range(n):
#    corrmat[i,i] = 1
 
#corrmat = corrmat.T@corrmat




#muvec = np.array([[.1,.07,.12,-0.03,0.11,-0.08,]]).T
#sigvec = np.array([[.2,.3,.22]]).T
#corrmat = np.array([[1.,.8,.2],[.8,1.,-0.3],[0.2,-0.3,1.]])

covmat = (sigvec@sigvec.T)*corrmat  # annualized covariance matrix
#SigMat = beta = np.array([[.2,0,0],[0,.3,0],[0,0,.22]])

beta = np.linalg.cholesky(covmat)*np.sqrt(dt)


alpha0 = muvec *dt
alpha1 = np.zeros((n,n))

# specify the causality structure
alpha1[0,3] = -0.2  # 3 affects 0
alpha1[0,8] = 0.6  # 8 affects 0
alpha1[2,8] = -0.1  # 8 affects 2
alpha1[3,2] = 0.5   # 2 affects 3
alpha1[8,3] = 0.4   # 3 affects 8
alpha1[9,2] = -0.5  # 2 affects 9
alpha1[8,8] = -0.1  # 8 affects 8

#alpha1 = np.array([[0., 0, 0.5],[0.5,0,0],[-0.5,0,0]])
#beta = np.array([[1,0,0],[0,1,0],[0,0,1]])




r = sim.simVAR2(alpha0, alpha1, beta, x0, numtimesteps, numsims, Cov = None, panel_out = False)

r = r[0,:,:]

dr = pd.DataFrame(r)

dS = (1+dr).cumprod(axis = 0)
#dS.columns = ['Stock 1', 'Stock 2', 'Stock 3']

dS.plot()

#%%
#dS.to_csv('C:/Users/japri/OneDrive/Python/MyCode/BYU Project/Network Reconstruction/SimStockData2.csv')
dS.to_csv('C:/Users/Jim/OneDrive/Python/MyCode/BYU Project/Network Reconstruction/SimStockData2.csv')

